commands_description = {
    "uptime": "returns server uptime",
    "info": "returns the version number of the server and the date it was created ",
    "help": "returns the list of available commands with a short description",
    "stop": "stops the server and the client simultaneously",
}

options_to_start = ["login", "add-admin"]
possible_options_for_admin = [
    "info",
    "help",
    "stop",
    "uptime",
    "list_of_users",
    "add_user",
    "logout",
]
possible_options_for_user = ["info", "help", "uptime", "send", "logout", "show_inbox"]
