import socket as s
import commands
import json
from client_config import Client

client = Client("192.168.0.115", 61033, 1024)

client_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
client_socket.connect((client.HOST, client.PORT))


def send_to_server(message):
    client_socket.send(message.encode("utf8"))


def recv_to_server():
    return client_socket.recv(client.BUFFER).decode("utf8")


def admin_options():
    client_request = input("Enter your command: ")
    send_to_server(client_request)
    if client_request == "stop":
        server_answer = recv_to_server()
        print(server_answer)
        client_socket.close()
        return "stop"
    elif client_request == "add_user":
        new_user_nickname = input("Input user nickname: ")
        send_to_server(new_user_nickname)
        server_answer = recv_to_server()
        print(server_answer)

    elif client_request == "logout":
        server_answer = recv_to_server()
        print(server_answer)
        return "logout"

    else:
        server_answer = recv_to_server()
        print(server_answer)
    return None


def user_options(client_login):
    client_request = input("Enter your command: ")
    send_to_server(client_request)

    if client_request == "This command is not for user":
        server_answer = recv_to_server()
        print(server_answer)
    elif client_request == "send":
        sender = client_login
        recipient = input("Enter recipient's nickname: ")
        message = input("Enter your message: ")
        send_to_server(sender)
        send_to_server(recipient)
        send_to_server(message)
        server_answer = recv_to_server()
        print(server_answer)
    elif client_request == "show_inbox":
        server_answer = recv_to_server()
        print(server_answer)

    elif client_request == "logout":
        server_answer = recv_to_server()
        print(server_answer)
        return "logout"

    else:
        server_answer = recv_to_server()
        print(server_answer)

    return None


while True:
    client_request = input("Enter your command: ")
    if client_request in commands.options_to_start:
        if client_request == "login":
            send_to_server(client_request)
            client_login = input("Input your username: ")
            client_password = input("Input your password: ")
            send_to_server(client_login)
            send_to_server(client_password)

            server_response = recv_to_server()

            if server_response == "Correct_login_and_password":
                if client_login == "admin":
                    print(f"Hello {client_login}")
                    server_request = recv_to_server()
                    print(server_request)
                    while True:
                        result = admin_options()
                        if result in ["stop", "logout"]:
                            break

                else:
                    print(f"Hello {client_login}")
                    server_request = recv_to_server()
                    print(server_request)
                    while True:
                        result = user_options(client_login)
                        if result in ["wrong", "logout"]:
                            break

            else:
                server_answer = recv_to_server()
                print(server_answer)
                client_socket.close()
                break

        elif client_request == "add-admin":
            send_to_server(client_request)
            new_user_nickname = input("Input admin nickame: ")
            client_socket.send(new_user_nickname.encode("utf8"))
            server_answer = recv_to_server()
            print(server_answer)

        else:
            server_answer = recv_to_server()
            print(server_answer)
            client_socket.close()
            break
    else:
        send_to_server(client_request)
        server_answer = recv_to_server()
        print(server_answer)
