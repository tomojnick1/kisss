import socket as s
import commands
import json
from functions_server import Server

server = Server("192.168.0.115", 61033, 1024)


server_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
server_socket.bind((server.HOST, server.PORT))
server_socket.listen(2)
client_socket, address = server_socket.accept()


def send_to_client(message):
    server_answer = json.dumps(message)
    client_socket.send(server_answer.encode("utf8"))


def recv_from_client():
    return client_socket.recv(server.BUFFER).decode("utf8")


def admin_options():
    client_answer = recv_from_client()
    if client_answer in commands.possible_options_for_admin:
        if client_answer == "info":
            send_to_client(server.info_config())
        elif client_answer == "uptime":
            send_to_client({"server_uptime": str(server.uptime_config())})
        elif client_answer == "help":
            send_to_client(commands.commands_description)
        elif client_answer == "list_of_users":
            send_to_client(server.list_of_users())
        elif client_answer == "add_user":
            new_user_nickname = recv_from_client()
            send_to_client(server.add_user(new_user_nickname))
        elif client_answer == "logout":
            send_to_client("You are logout!")
            print("Admin are logout")
            return "logout"
        elif client_answer == "stop":
            send_to_client("Bye!")
            # client_socket.close()
            server_socket.close()
            return "stop"
    else:
        send_to_client("This command doesn't exists, try once again")
    return None


def user_options():
    client_answer = recv_from_client()
    if client_answer in commands.possible_options_for_user:
        if client_answer == "info":
            send_to_client(server.info_config())
        elif client_answer == "uptime":
            send_to_client({"server_uptime": str(server.uptime_config())})
        elif client_answer == "help":
            send_to_client(commands.commands_description)
        elif client_answer == "logout":
            send_to_client("You are logout!")
            print("User are logout")
            return "logout"
        elif client_answer == "send":
            sender = recv_from_client()
            recipient = recv_from_client()
            message = recv_from_client()
            send_to_client(server.send_message(sender, recipient, message))
        elif client_answer == "show_inbox":
            list_of_inbox = server.show_list_of_inbox()
            send_to_client(list_of_inbox)

        else:
            send_to_client("This command doesn't exists, try once again")
    else:
        send_to_client("This command is not for user")

    return None


while True:
    client_answer = recv_from_client()
    if client_answer in commands.options_to_start:
        if client_answer == "login":
            client_login = recv_from_client()
            client_password = recv_from_client()

            if server.login(client_login, client_password):
                print(f"User {client_login} logged in successfully.")
                server_answer_about_login = "Correct_login_and_password"
                client_socket.send(server_answer_about_login.encode("utf8"))

                if client_login == "admin":
                    send_to_client(
                        f"You have possible options like: {commands.possible_options_for_admin}"
                    )

                    while True:
                        result = admin_options()
                        if result in ["logout", "stop"]:
                            break
                else:
                    print("Hello user!")
                    send_to_client(
                        f"You have possible options like: {commands.possible_options_for_user}"
                    )
                    while True:
                        result = user_options()
                        if result in ["wrong", "logout"]:
                            break

            else:
                print(f"Login failed for user {client_login}.")
                client_socket.close()
                break

        elif client_answer == "add-admin":
            new_user_nickname = recv_from_client()
            server_answer = server.add_user(new_user_nickname, type="admin")
            client_socket.send(server_answer.encode("utf8"))

        else:
            send_to_client("Wrong login or password")

    else:
        send_to_client("This command doesn't exist, try once again")
