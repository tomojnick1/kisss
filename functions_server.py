from datetime import datetime
import random
import string


class Server:
    def __init__(self, HOST="192.168.0.115", PORT=61033, BUFFER=1024):
        self.HOST = HOST
        self.PORT = PORT
        self.BUFFER = BUFFER
        self.creation_time = datetime.now()
        self.version = "Version 0.1.0"
        self.users = {}
        self.list_of_inbox = []

    def uptime_config(self):
        current_time = datetime.now()
        server_uptime = current_time - self.creation_time
        server_time = {"server_time": str(server_uptime)}
        return server_time

    def info_config(self):
        version = {
            "Version": self.version,
            "Version_date": datetime.now().strftime("%m/%d/%Y, %H:%M"),
        }
        return version

    def add_user(self, new_nickname, type="user"):
        if type == "admin" and any(
            user["type"] == "admin" for user in self.users.values()
        ):
            return "Admin is exists, choose another nickname"

        elif self.get_users(new_nickname):
            return "Choose another nickname"
        else:
            password = self.generate_password()
            user_data = {"nickname": new_nickname, "password": password, "type": type}
            self.users[new_nickname] = user_data
            return f"The User {new_nickname} has been added. Password: {password}, rank : {type}"

    def get_users(self, nickname):
        return self.users.get(nickname, None)

    def generate_password(self):
        characters = string.ascii_letters + string.digits + string.punctuation
        password = "".join(random.choice(characters) for x in range(4))
        return password

    def login(self, nickname, password):
        user = self.users.get(nickname)
        if user and user["password"] == password:
            return True
        else:
            return False

    def list_of_users(self):
        user_list = []
        for user, data in self.users.items():
            info = f"Current users in the database: Username: {user}, Password: {data['password']}, Type: {data['type']}"
            user_list.append(info)

        return user_list

    def send_message(self, sender, recipient, message):
        if sender not in self.users or recipient not in self.users:
            return f"Error: Users not found"

        message_entry = {"sender": sender, "recipient": recipient, "message": message}
        if len(self.list_of_inbox) >= 5:
            self.list_of_inbox.pop(0)
        if sender == recipient:
            return "Error: Cannot send message to self"
        self.list_of_inbox.append(message_entry)
        return f"Message sent from {sender} to {recipient}"

    def show_list_of_inbox(self):
        return self.list_of_inbox
